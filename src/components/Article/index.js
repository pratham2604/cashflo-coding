import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

const Article = ({ title, list, cta, imageSrc, reverse, ctaLink }) => {
  return (
    <div className='article-container'>
      {!reverse ? (
        <>
          <TextContainer title={title} list={list} cta={cta} ctaLink={ctaLink} />
          <div className='article-image-container'>
            <img src={imageSrc} alt="article" className='article-image'/>
          </div>
        </>
      ) : (
        <>
          <div className='article-image-container'>
            <img src={imageSrc} alt="article" className='article-image'/>
          </div>
          <TextContainer title={title} list={list} cta={cta} ctaLink={ctaLink} className='article-image'/>
        </>
      )}
    </div>
  );
};

const TextContainer = ({ title, list, cta, ctaLink }) => {
  return (
    <div className='text-container'>
      <div>{title}</div>
      <div>{list.map((item, index) => <div key={index}>{item}</div>)}</div>
      {cta && <div>
        <Link to={ctaLink}>{cta}</Link>
      </div>}
    </div>
  );
}

export default Article;