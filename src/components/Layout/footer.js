import React from 'react';
import { Link } from 'react-router-dom';
import Logo from './../../assets/logo.png';

const FOOTER_COLUMNS = [{
  title: 'Product',
  list: [{
    title: 'Video Resume',
    link: '/video-resume'
  }, {
    title: 'Contact Us',
    link: '/contact-us'
  }]
}, {
  title: 'Resources',
  list: [{
    title: 'Good Resume',
    link: '/good-resume'
  }, {
    title: 'Bad Resume',
    link: '/bad-resume'
  }, {
    title: 'Good Resume',
    link: '/good-resume'
  }, {
    title: 'Bad Resume',
    link: '/bad-resume'
  }]
}]

const Footer = () => {
  return (
    <div className='footer-container'>
      <div className='desktop-container'>
        <div className='desktop-footer-container'>
          <FooterContainer />
        </div>
      </div>
      <div className='mobile-container'>
        <div className='mobile-footer-container'>
          <FooterContainer />
        </div>
      </div>
      <div className='copy-right'>
        <p>© 2021 Awaz. All rights reserved.</p>
      </div>
    </div>
  );
};

const FooterContainer = () => (
  <>
    <div className='logo-container'>
      <Link to="/">
        <img src={Logo} alt="logo" className='logo'/>
      </Link>
    </div>
    {FOOTER_COLUMNS.map((column, index) => (
      <FooterColumn key={index} title={column.title} list={column.list} />
    ))}
  </>
)

const FooterColumn = ({ title, list }) => {
  return (
    <div className='footer-column'>
      <h3 className='footer-title'>{title}</h3>
      {list.map((item, index) => (
        <div key={index} className='footer-item'>
          <Link to={item.link} className='footer-link awaz-link'>{item.title}</Link>
        </div>
      ))}
    </div>
  );
}

export default Footer;