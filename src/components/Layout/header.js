import React from 'react';
import { Link } from 'react-router-dom';
import Logo from './../../assets/logo.png';

const Header = () => {
  return (
    <div className='header-container'>
      <div className='desktop-container'>
        <div className='desktop-header-container'>
          <div className='logo-container'>
            <Link to="/">
              <img src={Logo} alt="logo" className='logo'/>
            </Link>
          </div>
          <div className='link-container'>
            <Link className='link' to="/recruiters">Recruiters</Link> 
          </div>
          <div className='link-container'>
            <Link className='link' to="/blogs">Blogs</Link>
          </div>
          <div>
            <button className='awaz-button'>Get Started</button>
          </div>
        </div>
      </div>
      <div className='mobile-container'>
        <div className='mobile-header-container'>
          <div className='logo-container'>
            <Link to="/">
              <img src={Logo} alt="logo" className='logo'/>
            </Link>
          </div>
          <div className='menu-container'>
            Menu
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;