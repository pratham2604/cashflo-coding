import React from 'react';
import Article from '../../components/Article';
// import Dummy from '../../assets/dummy.png';

const Article1 = {
  title: <h3>Get Conventional video in 5 mins for article 1</h3>,
  list: [
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
  ],
  cta: 'Get started',
  ctaLink: '/video-resume',
  imageSrc: 'https://i.imgur.com/oFIWV3k.jpeg'
}

const Article2 = {
  title: <h3>Get Conventional video in 5 mins for article 2</h3>,
  list: [
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
  ],
  cta: 'Get started',
  ctaLink: '/video-resume',
  imageSrc: 'https://i.imgur.com/oFIWV3k.jpeg'
}

const Article3 = {
  title: <h3>Get Conventional video in 5 mins for article 3</h3>,
  list: [
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
    'Conventional video in 5 mins checklist',
  ],
  cta: 'Get started',
  ctaLink: '/video-resume',
  imageSrc: 'https://i.imgur.com/oFIWV3k.jpeg'
}

const Home = () => {
  return (
    <div>
      <div>Landing page</div>
      <Article title={Article1.title} list={Article1.list} cta={Article1.cta} ctaLink={Article1.ctaLink} imageSrc={Article1.imageSrc} />
      <Article title={Article2.title} list={Article2.list} cta={Article2.cta} ctaLink={Article2.ctaLink} imageSrc={Article2.imageSrc} reverse />
      <Article title={Article3.title} list={Article3.list} imageSrc={Article3.imageSrc} />
      <div>Title video</div>
      <div>SlideShow</div>
      <div>container</div>
    </div>
  );
};

export default Home;