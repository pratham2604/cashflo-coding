import React from "react";

import { createBrowserRouter } from "react-router-dom";

import Home from "../../containers/home/index";
import Recruiters from "../../containers/recruiters";

import Layout from "../../components/Layout";

const routes = createBrowserRouter([
  {
    path: "/",
    element: (
      <Layout>
        <Home />
      </Layout>
    ),
  },
  {
    path: "/recruiters",
    element: (
      <Layout>
        <Recruiters />
      </Layout>
    ),
  },
]);

export default routes;
