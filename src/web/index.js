import React from "react";

import { RouterProvider } from "react-router-dom";

import routes from "./routes/index";
import './styles/global.css';

const Index = () => {
  return (
    <RouterProvider router={routes} />
  );
};

export default Index;
